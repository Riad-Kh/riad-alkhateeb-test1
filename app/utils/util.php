<?php
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Auth;

function uploadImage($image , $fileName)
{
    $rand = rand(0 , 9999999);
    $imageName =  time() . $rand .'.'. $image->extension();
    $imageToSave = $fileName . '/' . time(). $rand . '.'. $image->extension();
    $image->move(public_path('storage'. DIRECTORY_SEPARATOR .$fileName), $imageName);
    return $imageToSave;
}

function getImage($image)
{
    if(isset($image) && !empty($image)) {
        $image = str_replace('\\\\', '/', $image);
        $image = str_replace('\\', '/', $image);
        return URL::to('/') . '/storage/' . $image ;
    }
    else return null;
}
function user() {
    return Auth::guard('web')->user();
}
