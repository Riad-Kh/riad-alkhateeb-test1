<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class LoginController extends Controller
{
    public function login(Request $request){
        try{
                $user = User::query()->where('phone',$request->phone)->first();

                if($user->is_conferm==1)
                    return 'your form under review';

                if(Auth::guard('web')->attempt(['phone' => $request->phone , 'password' => $request->password])){
                        $user->is_conferm = 1;
                        $user->save();
                        return redirect('user-form');
                }
       }catch(\Exception $ex){
            return $ex->getMessage();
        }
      }

}

