<?php

namespace App\Http\Controllers;

use App\Mail\UserEmail;
use App\Models\User;
use App\Models\UserAccommodation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class UserFormController extends Controller
{
    public function showForm()
    {
        return view('user-form');
    }

    public function createForm(Request $request)
    {
        try{

            $user=user();
            $user->is_conferm=1;
            $user->conferm_code=null;
            $user->update($request->all());

            return redirect('accommodation');

        }catch(\Exception $ex){
            return $ex->getMessage();
        }
}

    public function showAccommodation()
    {
        return view('accommodation');
    }
    public function createAcommodation(Request $request)
    {
        try{
            $request['user_id'] = user()->id;

            UserAccommodation::create($request->all());

            $mailData="Well Done
            Your information has been submitted successfully
            You will receive in coming day invitation email with instructions from RS4IT to book your flight.
            See you soon";

            Mail::to(user()->email)->send(new UserEmail($mailData));

            return redirect('done');

        }catch(\Exception $ex){
            return $ex->getMessage();
        }
    }

    public function done()
    {
        return view('done');
    }
}
