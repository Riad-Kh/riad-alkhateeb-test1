<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('first_name');
            $table->string('last_name');
            $table->integer('conferm_code')->nullable();
            $table->boolean('is_conferm');
            $table->date('date_of_birth');
            $table->string('gender');
            $table->string('place_of_birth');
            $table->string('country_of_residency');
            $table->string('passport_no');
            $table->date('issue_date');
            $table->date('expiry_date');
            $table->string('place_of_issue');
            $table->date('arrival_date');
            $table->string('profession');
            $table->string('organization');
            $table->integer('visa_duration');
            $table->string('visa_status');
            $table->string('email')->unique();
            $table->string('phone')->nullable();
            $table->string('personal_image')->nullable();
            $table->string('passport_image')->nullable();
            $table->string('password');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
