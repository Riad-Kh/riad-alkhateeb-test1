<?php

use App\Http\Controllers\LoginController;
use App\Http\Controllers\UserFormController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
});

Route::post('/login',[LoginController::class,'login'])->name('login');

Route::middleware('auth:web')->group(function () {
    Route::get('/user-form',[UserFormController::class,'showForm'])->name('user-form');
    Route::Post('/submit-form',[UserFormController::class,'createForm'])->name('submit-form');
    Route::get('/accommodation', [UserFormController::class,'showAccommodation'])->name('accommodation');
    Route::post('/submit-accommodation',[UserFormController::class,'createAcommodation'])->name('submit-accommodation');
    Route::get('/done',[UserFormController::class,'done'])->name('done');

});






Route::get('test', function (){
   return   Hash::make(12345);
});
