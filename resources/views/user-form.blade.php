<!DOCTYPE html>
<html>

<head>
    <title>Account registration form</title>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600' rel='stylesheet'
        type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>

    <link href="{{ asset('/css/user-form.css') }}" rel="stylesheet">


</head>

<body>
    <div class="main-block">
        <form  method="post" action="{{ route('submit-form') }}"  enctype="multipart/form-data">
            @csrf
            <h1 class = "title">Passport Informations</h1>
            <fieldset>
                <legend>
                    <h3>Personal Details</h3>
                </legend>
                <div class="personal-details">
                    <div>
                        <div><label>First Name*</label><input type="text" name="first_name" required></div>
                        <div><label>Last Name*</label><input type="text" name="last_name" required></div>

                        <div>
                            <label>Country Of Birth</label>
                            <select name = "place_of_birth">
                                <option value="Armenia">Armenia</option>
                                <option value="Russia">Russia</option>
                                <option value="Germany">Germany</option>
                                <option value="France">France</option>
                                <option value="USA">USA</option>
                                <option value="UK">UK</option>
                            </select>
                        </div>

                        <div>
                            <label>Place Of Residency </label>
                            <select name = "country_of_residency">
                                <option value="Armenia">Armenia</option>
                                <option value="Russia">Russia</option>
                                <option value="Germany">Germany</option>
                                <option value="France">France</option>
                                <option value="USA">USA</option>
                                <option value="UK">UK</option>
                            </select>
                        </div>


                        <div><label>Date Of Birth*</label><input type="date" name="date_of_birth" min="<?php ?>" required></div>

                        <div><label>Passport Number*</label><input type="number" name="passport_no" required></div>


                        <div>
                            <label>Gender*</label>
                            <div class="gender">
                                <input type="radio" value="male" id="male" name="gender" required />
                                <label for="male" class="radio">Male</label>
                                <input type="radio" value="female" id="female" name="gender" required />
                                <label for="female" class="radio">Female</label>
                            </div>
                        </div>

                    </div>
                    <div>


                        <div><label>Issue Date*</label><input type="date" name="issue_date" required></div>
                        <div><label>Expiry Date*</label><input type="date" name="expiry_date" required></div>

                        <div>
                            <label>Place  Of Issue</label>
                            <select name = "place_of_issue">
                                <option value="Armenia">Armenia</option>
                                <option value="Russia">Russia</option>
                                <option value="Germany">Germany</option>
                                <option value="France">France</option>
                                <option value="USA">USA</option>
                                <option value="UK">UK</option>
                            </select>
                        </div>

                        <div><label>Arrival date*</label><input type="date" name="arrival_date" required></div>

                        <div><label>Profession</label><input type="text" name="profession" ></div>

                        <div><label>Organization</label><input type="text" name="organization" ></div>

                    </div>
                </div>
            </fieldset>
            <fieldset>
                <legend>
                    <h3>Visa Details</h3>
                </legend>
                <div class="personal-details">
                    <div>
                        <div>
                            <label>Visa duration </label>
                            <select name = "visa_duration">
                                <option value="01">01</option>
                                <option value="02">02</option>
                                <option value="03">03</option>
                                <option value="04">04</option>
                                <option value="05">05</option>
                                <option value="06">06</option>
                                <option value="07">07</option>
                                <option value="08">08</option>
                                <option value="09">09</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                                <option value="13">13</option>
                                <option value="14">14</option>
                                <option value="15">15</option>
                                <option value="16">16</option>
                                <option value="17">17</option>
                                <option value="18">18</option>
                                <option value="19">19</option>
                                <option value="20">20</option>
                                <option value="21">21</option>
                                <option value="22">22</option>
                                <option value="23">23</option>
                                <option value="24">24</option>
                                <option value="25">25</option>
                                <option value="26">26</option>
                                <option value="27">27</option>
                                <option value="28">28</option>
                                <option value="29">29</option>
                                <option value="30">30</option>
                                <option value="31">31</option>
                            </select>
                        </div>

                        <div>
                            <label>Visa status  </label>
                            <select name = "visa_status">
                                <option value="multiple">multiple </option>
                                <option value="single">single </option>
                            </select>
                        </div>
                    </div>
                    <div>
                        <div><label>Passport picture*</label><input type="file" name="passport_image" required></div>
                        <div><label>Personal picture*</label><input type="file" name="personal_image" required></div>

                    </div>
                </div>
            </fieldset>


            {{-- <button id= "show">Companion</button>
            <div id = "companion">

                        <h1 class = "title">Companion Informations</h1>
                    <fieldset>
                        <legend>
                            <h3>Personal Details</h3>
                        </legend>
                        <div class="personal-details">
                            <div>
                                <div><label>First Name*</label><input type="text" name="c-firstname" required></div>
                                <div><label>Last Name*</label><input type="text" name="c-lastname" required></div>

                                <div>
                                    <label>Country Of Birth</label>
                                    <select name = "c-country-of-birth">
                                        <option value="Armenia">Armenia</option>
                                        <option value="Russia">Russia</option>
                                        <option value="Germany">Germany</option>
                                        <option value="France">France</option>
                                        <option value="USA">USA</option>
                                        <option value="UK">UK</option>
                                    </select>
                                </div>

                                <div>
                                    <label>Place Of Residency </label>
                                    <select name = "c-Place-of-residency">
                                        <option value="Armenia">Armenia</option>
                                        <option value="Russia">Russia</option>
                                        <option value="Germany">Germany</option>
                                        <option value="France">France</option>
                                        <option value="USA">USA</option>
                                        <option value="UK">UK</option>
                                    </select>
                                </div>


                                <div><label>Date Of Birth*</label><input type="date" name="c-date-of-birth" required></div>

                                <div><label>Passport Number*</label><input type="number" name="c-passport-no" required></div>


                                <div>
                                    <label>Gender*</label>
                                    <div class="gender">
                                        <input type="radio" value="male" id="male" name="c-gender" required />
                                        <label for="male" class="radio">Male</label>
                                        <input type="radio" value="female" id="female" name="c-gender" required />
                                        <label for="female" class="radio">Female</label>
                                    </div>
                                </div>

                            </div>
                            <div>


                                <div><label>Issue Date*</label><input type="date" name="c-issue-of-birth" required></div>
                                <div><label>Expiry Date*</label><input type="date" name="c-expiry-of-birth" required></div>

                                <div>
                                    <label>Place  Of Issue</label>
                                    <select name = "c-palce-of-issue">
                                        <option value="Armenia">Armenia</option>
                                        <option value="Russia">Russia</option>
                                        <option value="Germany">Germany</option>
                                        <option value="France">France</option>
                                        <option value="USA">USA</option>
                                        <option value="UK">UK</option>
                                    </select>
                                </div>

                                <div><label>Arrival date*</label><input type="date" name="c-arrival-date" required></div>

                                <div><label>Profession</label><input type="text" name="c-profession" ></div>

                                <div><label>Organization</label><input type="text" name="c-organization" ></div>

                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <legend>
                            <h3>Visa Details</h3>
                        </legend>
                        <div class="personal-details">
                            <div>
                                <div>
                                    <label>Visa duration </label>
                                    <select name = "c-visa-duration">
                                        <option value="01">01</option>
                                        <option value="02">02</option>
                                        <option value="03">03</option>
                                        <option value="04">04</option>
                                        <option value="05">05</option>
                                        <option value="06">06</option>
                                        <option value="07">07</option>
                                        <option value="08">08</option>
                                        <option value="09">09</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                        <option value="12">12</option>
                                        <option value="13">13</option>
                                        <option value="14">14</option>
                                        <option value="15">15</option>
                                        <option value="16">16</option>
                                        <option value="17">17</option>
                                        <option value="18">18</option>
                                        <option value="19">19</option>
                                        <option value="20">20</option>
                                        <option value="21">21</option>
                                        <option value="22">22</option>
                                        <option value="23">23</option>
                                        <option value="24">24</option>
                                        <option value="25">25</option>
                                        <option value="26">26</option>
                                        <option value="27">27</option>
                                        <option value="28">28</option>
                                        <option value="29">29</option>
                                        <option value="30">30</option>
                                        <option value="31">31</option>
                                    </select>
                                </div>

                                <div>
                                    <label>Visa status  </label>
                                    <select name = "c-visa-status">
                                        <option value="multiple">multiple </option>
                                        <option value="single">single </option>
                                    </select>
                                </div>
                            </div>
                            <div>
                                <div><label>Passport picture*</label><input type="file" name="c-passport-image" required></div>
                                <div><label>Personal picture*</label><input type="file" name="c-personal-image" required></div>

                            </div>
                        </div>
                    </fieldset>
            </div> --}}
            <button type="submit" href="/">Submit</button>
        </form>

    </div>

    <script>
     $(document).ready(function(){
        var companion = $('#companion');
  $("#show").click(function(){
    $("div").append('test');
  });

});
    </script>
</body>

</html>
