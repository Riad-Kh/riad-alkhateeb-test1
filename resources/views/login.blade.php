
<!DOCTYPE html>
<html lang="en">
 <head>
   <title>Login</title>
   <meta name="viewport" content="width=device-width, initial-scale=1" />
   <link
     rel="stylesheet"
     href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/css/intlTelInput.css"
   />
   <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/js/intlTelInput.min.js"></script>
   <link href="{{ asset('/css/login.css') }}" rel="stylesheet">
 </head>
 <body>
    <div class="login">
        <h1>Login</h1>
        <form method="post" action="{{ route('login') }}" name = "loginForm" onsubmit="return validateForm()">
            @csrf
            <input  id="phone" type="tel" placeholder="Phone" name="phone" /><br>
            <input type="password" name="password" placeholder="Password" required="required" />
            <button type="submit" class="btn btn-primary btn-block btn-large">Let me in.</button>
        </form>
    </div>

 </body>
  <script>
   const phoneInputField = document.querySelector("#phone");
   const phoneInput = window.intlTelInput(phoneInputField, {
     utilsScript:
       "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/js/utils.js",
   });
 </script>

<script>
    function validateForm() {
      let x = document.forms["myForm"]["Password"].value;
      var letters = /^[A-Za-z]+$/;
      if(!x.match(letters))
      {
      alert('Your name have accepted : you can try another');
      return true;
      }
      else
      {
      alert('Please input alphabet characters only');
      return false;
      }
    }
    </script>
</html>

