
<!DOCTYPE html>
<html lang="en">
 <head>
   <title>Done</title>
   <meta name="viewport" content="width=device-width, initial-scale=1" />
   <link
     rel="stylesheet"
     href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/css/intlTelInput.css"
   />
   <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/js/intlTelInput.min.js"></script>
   <link href="{{ asset('/css/login.css') }}" rel="stylesheet">
 </head>
 <body>
    <div class="login">
            <h1>
                Well Done
            Your information has been submitted successfully
            You will receive in coming day invitation email with instructions from RS4IT to book your flight.
            See you soon
            </h1>

    </div>

 </body>

</html>

