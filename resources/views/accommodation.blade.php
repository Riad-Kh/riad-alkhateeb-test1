
<!DOCTYPE html>
<html lang="en">
 <head>
   <title>Accommodation</title>
   <meta name="viewport" content="width=device-width, initial-scale=1" />
   <link
     rel="stylesheet"
     href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/css/intlTelInput.css"
   />
   <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/js/intlTelInput.min.js"></script>
   <link href="{{ asset('/css/accommodation.css') }}" rel="stylesheet">
 </head>
 <body>
    <div class="login">
        <h1>Accommodation preference </h1>
        <form method="post" action="{{ route('submit-accommodation') }}" name = "accommodationForm" >
            @csrf
            <input type="date" placeholder="Check in date" name="check_in_date" required="required" /><br>
            <input type="date" name="check_out_date" placeholder="Check out date " required="required" /> <br>
            <div class="date">
                <label>Room type </label>
                <select name = "room_type">
                    <option value="Armenia">king bed</option>
                    <option value="Russia">twin bed</option>
                </select>
            </div>
            <button type="submit" class="btn btn-primary btn-block btn-large">submit</button>
        </form>
    </div>

 </body>

</html>

